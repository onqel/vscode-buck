# Change Log

All notable changes to the "vscode-buck" extension will be documented in this file.

Check [Keep a Changelog](http://keepachangelog.com/) for recommendations on how to structure this file.

## [Releases]

### 0.1.5

- Added command `buck.refresh` to rebuild target list.

### 0.1.4

- Make immutable copy of postTargetArgs array to prevent modification of configuration content. Fixes faulty Buck Run command.
- Hide Buck commands from command palette when no project is selected and loaded.

### 0.1.3

- Added progress bar to generation of `compile_commands.json`.

### 0.1.2

- Updated README.md
- Removed unused configuration options.
- Added option `buck.useProjectConfig` (default: true)
- Handle cancelled commands (`buck.query`, `buck.audit`)
- Added buck-icon for `WIP` action bar view.

### 0.1.1

- Fixed error in `buck.projectConfig`.
- Added option to add output folder of generated `compile_commands.json` as argument to `vscode-clangd`.

### 0.1.0

- Fixed a couple of missing commas in snippets.
- Added additional configuration/options for generation of `compile-commands.json`.

### 0.0.9

- Code refactoring
- Added config `buck.processCompilationDatabase` to replace include directives such as `"@/home/john/freetype2.txt"` with its content i.e `"-lfreetype"`

### 0.0.8

- Compile project using webpack
- Re-implemented `buck.generateCompilationDatabase` command.

### 0.0.7

- Fix faulty buck run command.

### 0.0.6

- Activate extension only if .buckconfig file found in workspace.
- Prepend "#" to values of `buck.platform`.

### 0.0.5

- Switched to markdownDescription for `buck.configArgs`

### 0.0.4

- Fixed missing dependency.

### 0.0.3

- Added license.

### 0.0.2

- Added Buck icon.

### 0.0.1

- Initial release.
- Initial release
