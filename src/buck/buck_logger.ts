import * as vscode from "vscode";
import { getBuckAutoFocusOutputChannel } from "./buck_config";

class BuckOutputChannel {
    private outputChannel: vscode.OutputChannel;

    constructor() {
        this.outputChannel = vscode.window.createOutputChannel("Buck");
    }

    append(value: string) {
        this.outputChannel.append(value);
    }

    appendLine(value: string) {
        this.outputChannel.appendLine(value);
    }

    clear() {
        this.outputChannel.clear();
    }

    dispose() {
        this.outputChannel.dispose();
    }

    hide() {
        this.outputChannel.hide();
    }

    show(preserveFocus?: boolean | undefined) {
        if (getBuckAutoFocusOutputChannel()) {
            this.outputChannel.show(preserveFocus);
        }
    }

    get name(): string {
        return this.outputChannel.name;
    }
}


export const outputChannel = new BuckOutputChannel();
