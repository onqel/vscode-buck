import * as vscode from "vscode";


interface ICompilationDatabaseSettings {
    outputFolder: string;
    flatten: boolean;
    depth: number;
    updateClangd: boolean;
    openGeneratedFile: boolean;
}

export function getCompilationDatabaseSettings(): ICompilationDatabaseSettings {
    const config = getConfig();
    let cdbConfig = {
        outputFolder: config.compilationDatabase.outputFolder as string,
        depth: config.compilationDatabase.dependencyDepth as number,
        flatten: config.compilationDatabase.flatten as boolean,
        updateClangd: config.compilationDatabase.updateClangdCompileCommandsDir as boolean,
        openGeneratedFile: config.compilationDatabase.openGeneratedFile as boolean
    };

    if (cdbConfig.outputFolder.length > 1) {
        if (cdbConfig.outputFolder.endsWith('/')) {
            cdbConfig.outputFolder = cdbConfig.outputFolder.substring(0, cdbConfig.outputFolder.lastIndexOf('/'));
        }
    } else {
        if (vscode.workspace.workspaceFolders) {
            cdbConfig.outputFolder = vscode.workspace.workspaceFolders[0].uri.fsPath;
        }
    }

    return cdbConfig;
}

export async function updateClangd() {
    const clangd = vscode.extensions.getExtension("llvm-vs-code-extensions.vscode-clangd");

    if (clangd) {
        const clangd_config = vscode.workspace.getConfiguration("clangd");

        if (clangd_config) {
            const cdb = getCompilationDatabaseSettings();
            let clangd_args = clangd_config.arguments as string[];

            clangd_args = clangd_args
                .filter((arg: string) => !arg.startsWith("-compile-commands-dir="))
                .concat(`-compile-commands-dir=${cdb.outputFolder}`);

            await clangd_config.update("arguments", clangd_args);
        }
    }
}

export function getConfig(): vscode.WorkspaceConfiguration {
    return vscode.workspace.getConfiguration("buck");
}

export function getDefaultBuckExecutable(): string {
    const config = getConfig();
    const buckExecutable = config.executable as string;
    if (buckExecutable.length === 0) {
        return "buck";
    }
    return buckExecutable;
}

export function getPlatformFlavor() {
    const config = getConfig();
    const platform = process.platform;

    if ((config.platform as string).length > 0) {
        return '#' + config.platform as string;
    }

    switch (platform) {
        case 'win32':
            return "#windows-x86_64";
        case 'linux':
            return "#linux-x86_64";
        case 'darwin':
            return "#macosx-x86_64";
    }

    return "default";
}

export function getBuckConfigurationArgs() {
    const config = getConfig();
    return (config.configArgs as string[]).map(x => "-c " + x);
}

export function getPreTargetArgs(): string[] {
    const config = getConfig();
    const preTargetArgs = config.preTargetArgs as string[];
    return preTargetArgs;
}

export function getPostTargetArgs(): string[] {
    const config = getConfig();
    const postTargetArgs = config.postTargetArgs as string[];
    return postTargetArgs;
}

export function getBuckPipeStdErr() {
    const config = getConfig();
    const pipeStdErr = config.pipeStderr as boolean;
    return pipeStdErr;
}

export function getBuckAutoFocusOutputChannel() {
    const config = getConfig();
    return config.autoFocusOutputChannel as boolean;
}

interface BuckWorkspaceState {
    projectPath: string;
    target: string;
    flavor: string[];
    platform?: string;
}

export function shouldUseWorkspaceState() {
    const config = getConfig();
    return config.useProjectConfig as boolean;
}

export function getWorkspaceState(): BuckWorkspaceState | null {
    const config = getConfig();
    let proj = config.projectConfig as BuckWorkspaceState;

    return proj;
}

export function storeWorkspaceState(state: BuckWorkspaceState) {
    const config = getConfig();
    config.update("projectConfig", state, vscode.ConfigurationTarget.Workspace);
}
